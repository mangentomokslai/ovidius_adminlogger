<?php
class Ovidius_AdminLogger_Model_Observer
{
    public function controllerActionPredispatch($observer)
    {
        if (!Mage::getStoreConfigFlag('system/log/enable_admin_logger'))
            return;

        $user = Mage::getSingleton('admin/session')->getUser();
        if (is_object($user)) {
            $username = $user->getUsername();
        } else {
            $username = 'NOT LOGGED IN';
        }

        Mage::log(
            $username . ' | ' . Mage::app()->getRequest()->getPathInfo(),
            Zend_Log::INFO,
            'admin.log',
            true
        );

    }

}
